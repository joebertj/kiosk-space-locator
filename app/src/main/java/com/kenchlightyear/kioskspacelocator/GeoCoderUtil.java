package com.kenchlightyear.kioskspacelocator;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class GeoCoderUtil {
    double latitude = 14.5818;
    double longitude = 120.9770;
    LatLng latLng;
    boolean success = false;
    String message = null;

    public GeoCoderUtil(String location, Context context){
        List<Address> addressList = null;
        Geocoder geocoder = new Geocoder(context);

        try {
            addressList = geocoder.getFromLocationName(location, 1);
            Address address = addressList.get(0);
            latitude = address.getLatitude();
            longitude = address.getLongitude();
            latLng = new LatLng(latitude,longitude);
            success = true;
        }catch (Exception e){
            if(!(e instanceof IOException)) {
                success = true;
                message = "Either point of interest or address not found or search is too broad";
            }
        }
    }

    public String getMessage() {
        return message;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public boolean isSuccess() {
        return success;
    }
}
