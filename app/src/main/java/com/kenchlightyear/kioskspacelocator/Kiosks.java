package com.kenchlightyear.kioskspacelocator;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class Kiosks {
    List<Kiosk> kioskList = new ArrayList<Kiosk>();
    int results = 0;
    LatLng latLng;

    public Kiosks(double latitudeCenter, double longitudeCenter){
        latLng = new LatLng(latitudeCenter,longitudeCenter);
        String sURL = "https://liezel.kenchlightyear.com:3000/api/v1/kiosk";

        try {
            URL url = new URL(sURL + "?latitude=" + latitudeCenter + "&longitude=" + longitudeCenter);
            URLConnection request = url.openConnection();
            request.connect();
            JsonParser jp = new JsonParser();
            JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
            JsonArray kiosks = root.getAsJsonArray();
            for(JsonElement k: kiosks) {
                JsonObject ko = k.getAsJsonObject();
                String title = ko.get("title").getAsString();
                String description = ko.get("description").getAsString();
                double latitude = ko.get("latitude").getAsDouble();
                double longitude = ko.get("longitude").getAsDouble();
                int area = ko.get("area").getAsInt();
                double price = ko.get("price").getAsDouble();
                Kiosk kiosk = new Kiosk();
                kiosk.setTitle(title);
                kiosk.setDescription(description);
                kiosk.setLatLng(new LatLng(latitude,longitude));
                kiosk.setArea(area);
                kiosk.setPrice(price);
                kioskList.add(kiosk);
                results++;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public int getResults() {
        return results;
    }

    public List<Kiosk> getKioskList() {
        return kioskList;
    }

    public LatLng getLatLng() {
        return latLng;
    }
}
