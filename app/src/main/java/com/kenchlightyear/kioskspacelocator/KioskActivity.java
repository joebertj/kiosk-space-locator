package com.kenchlightyear.kioskspacelocator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

public class KioskActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kiosk);
        Intent intent = getIntent();
        String title= intent.getExtras().getString("title");
        String description= intent.getExtras().getString("description");
        int area = intent.getExtras().getInt("area");
        double price = intent.getExtras().getDouble("price");
        this.setTitle(title);
        TextView textView = (TextView) findViewById(R.id.textView);
        TextView textView2 = (TextView) findViewById(R.id.textView2);
        TextView textView3 = (TextView) findViewById(R.id.textView3);
        textView.setText(area + " sqm");
        textView2.setText("PHP " + String.format("%,.2f", price));
        textView3.setText(description);
    }
}
