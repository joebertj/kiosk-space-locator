package com.kenchlightyear.kioskspacelocator;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private HashMap<Marker, Kiosk> markerKioskHashMap = new HashMap<Marker, Kiosk>();
    private String message = null;
    private double latitude = 14.5818;
    private double longitude = 120.9770;
    private LatLng current = new LatLng(latitude,longitude);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        //StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.activity_maps);
        final Button search = (Button) findViewById(R.id.search_button);
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        search.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                hideKeyboardFrom(v.getContext(),v.getRootView());
                search.setClickable(false);
                search.setEnabled(false);
                progressBar.setVisibility(View.VISIBLE);
                mMap.clear();
                new MapSearch().execute(v);
            }
        });
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        try {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            }
        } catch (Exception e){
            e.printStackTrace();
        }

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Intent intent = new Intent(getApplicationContext(), KioskActivity.class);
                Kiosk kiosk = markerKioskHashMap.get(marker);
                intent.putExtra("title",kiosk.getTitle());
                intent.putExtra("description", kiosk.getDescription());
                intent.putExtra("area",kiosk.getArea());
                intent.putExtra("price",kiosk.getPrice());
                startActivity(intent);
            }
        });
        GpsTracker gpsTracker;

        gpsTracker = new GpsTracker(MapsActivity.this);
        if(gpsTracker.canGetLocation()){
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
        }else{
            gpsTracker.showSettingsAlert();
        }
        current = new LatLng(latitude, longitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(current,15));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17),2000,null);
        // Search for kiosks nearby GPS location
        new MapSearchInit().execute(current);

    }

    public class MapSearchInit extends AsyncTask<LatLng,Void,Kiosks>{
        @Override
        protected Kiosks doInBackground(LatLng [] latLngs) {
            return new Kiosks(latLngs[0].latitude,latLngs[0].longitude);
        }

        @Override
        protected void onPostExecute(Kiosks kiosks) {
            markerKioskHashMap.clear();
            if (kiosks.getResults() > 0) {
                for (Kiosk k : kiosks.getKioskList()) {
                    Marker m = mMap.addMarker(new MarkerOptions().position(k.getLatLng()).title(k.getTitle()));
                    markerKioskHashMap.put(m,k);
                }
            }
            //mMap.animateCamera(CameraUpdateFactory.newLatLng(kiosks.getLatLng()));
            Circle circle = mMap.addCircle(new CircleOptions().center(kiosks.getLatLng()).radius(1000).strokeColor(Color.RED));
            circle.setVisible(true);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(current,getZoomLevel(circle)));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(getZoomLevel(circle)), 2000, null);
        }
    }

    public class MapSearch extends AsyncTask<View,Void,Kiosks>{
        @Override
        protected Kiosks doInBackground(View [] view) {
            EditText locationSearch = (EditText) findViewById(R.id.editText);
            String location = locationSearch.getText().toString();
            GeoCoderUtil geoCoderUtil = new GeoCoderUtil("Rizal Monument, Philippines",view[0].getContext());
            if (location != null || !location.equals("")) {
                //new SearchKiosk(this).execute(location);
                geoCoderUtil = new GeoCoderUtil(location,view[0].getContext());
                while(!geoCoderUtil.success){
                    geoCoderUtil = new GeoCoderUtil(location,view[0].getContext());
                }
                message = geoCoderUtil.getMessage();
            }
            return new Kiosks(geoCoderUtil.getLatitude(),geoCoderUtil.getLongitude());
        }

        @Override
        protected void onPostExecute(Kiosks kiosks) {
            LatLng kiosksLatLng = kiosks.getLatLng();
            if(message != null){
                Toast toast = Toast.makeText(MapsActivity.this, message, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER,0,0);
                toast.show();
                message = null;
                kiosksLatLng = current;
            }
            markerKioskHashMap.clear();
            if (kiosks.getResults() > 0) {
                for (Kiosk k : kiosks.getKioskList()) {
                    Marker m = mMap.addMarker(new MarkerOptions().position(k.getLatLng()).title(k.getTitle()));
                    markerKioskHashMap.put(m,k);
                }
            }
            //mMap.animateCamera(CameraUpdateFactory.newLatLng(kiosks.getLatLng()));
            Circle circle = mMap.addCircle(new CircleOptions().center(kiosks.getLatLng()).radius(1000).strokeColor(Color.RED));
            circle.setVisible(true);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(kiosksLatLng,getZoomLevel(circle)));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(getZoomLevel(circle)), 2000, null);
            final Button search = (Button) findViewById(R.id.search_button);
            final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
            progressBar.setVisibility(View.INVISIBLE);
            search.setEnabled(true);
            search.setClickable(true);
        }
    }

    public float getZoomLevel(Circle circle) {
        float zoomLevel = 15;
        if (circle != null){
            double radius = circle.getRadius();
            double scale = radius / 500;
            zoomLevel =(float) (16 - Math.log(scale) / Math.log(2));
        }
        return zoomLevel;
    }

    private static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}
